
Banco de dados
#   Estudos

#  O que é um banco de dados?

#  Quem manipula o banco de dados 
    SGBD 
      Criar
      Excluir
      Inserir 
      Atualizar
#  Estrutura do Banco dados
    Todo banco tem uma estrutura que é conhecida como modelos de dados

#  Formas de representar o modelo de dados
    1 - Mod. Conceitual 
    2 - Mod. Lógico
    3 - Mod. Físico
    
    
#  Abordagem ER - cap-2
    Abordagem Entidade-Relacionamento (Estudar primeiras aulas do curso de banco de dados do Gustavo Guanabara)
  
      - Modelo de dados é reproresentado atrvés de um modelo ER - Teoria
    
      - DER - aplicação da Teoria 
      
#  Conceitos centrais de abordagem ER
    Entidade 
      Identificar os obj que se quer guardar alguma  determinada informação, esses objetos são conhecidos como ENTIDADES
        Ex.: Para uma  escola 
              Notas do aluno
              Nome 
              Serie
              ETC ...
        No diagrama é representado como um retengulo com o nome dentro
#  Entidade e Instância
  
#  Propriedades de entidades
    Em um modelos ER
      Relacionamentos 
      Atributos 
      Generalizada ou expecializada. 
      
#  Relacionamento - Conceito
    São conjuntos de associações entre as ocorrencias sobre as quais deseja manter as inf. na base de dados.
    
#  Diagrama de ocorrências
    	Auto relacionamento
    
#  Cardinalidade de relacionamento
    Esta relacionado ao numero de ocorrencias que estão ocorrendo
        São dois tipos de cardinalidade
        Tipos de Cardinalidade
          1:1 -> Um pra um
          1:N -> Um pra muitos
          N:N -> Muitos Para Muitos
          
#  Generalização/especialização 
	Quando duas entidades tem mais de duas ocorrencias(relacionamento) podemos utilizar o conceito de especialização

#  Generalização/ especialização(recursão)
	Proibido usar herança multiplas idetificadores

# Especialização Total
	
# Especialização parcial
	
# Entidade Associativa 

# Substituindo relacionamento por entidade
	Existe quando as ocorrencias não deveriam existir. 


#  Tipos primitivos BD
    numerico
      inteiro
        Tinyint, SmallInt, Int, MediumInt, BigInt
    real
      Decimal, Float, Double, Real
    logico
      Bit, Boolean


#  Data/Tempo
    Data, DataTime, TimeStamp, Time, Year

#  Literal
    Caractere
      Char, Varchar
#  Texto
    TinyText, Text, MediumText

    Binário
      TinyBlob, Blob, MediumBlob, LongBlob

    Coleção
      Enum, Set

#  Espacial
    Geometry, Point, Polygon, multiPolygon

#  Comandos
    Criar bancos
      create database nome_do_banco
      default character set utf8 //Aceitar acentos
      default collate utf8_general_ci;

#  Criar tabelas
    create table nome_da_tabela(
      id int not null auto_incremente
      nome Varchar(30) NOT NUll, //not null = constrante que obriga a informar o nome
      idade date,
      sexo enum("m", "f"), //obrigatório colocar f e m
      peso decimal(5,2), //5 casas ao todo e depois da vigula 2
      altura decimal(3,2)
      nacionalidade varchar(20) default 'brasil',
      PRIMARY KEY(id)
    ) default charset = utf8 //conf de caracter padrão na tabela

#  Chave Primaria
    para diferenciar de outros dados que possam ser adicionados mais de uma vez

#  DDL = comandos de definicão ex criar banco
        create database
        create table
        alter table
        drop table
#  DML = Comando para modificação
        insert into
        update
        delete
        truncate


#  Inserindo dados no bancos
    insira na tabela pessoas
    (id, nome, idade, sexo, nacionalidade)
    os valores

#  insert into pessoas
    (nome, idade, sexo, nacionalidade) values  (
    'Admin',
    '1995-1-12',
    'M',
    'Brasil'
    );

#  Alterando a estrutura de dados

#  Comando para alterar
      alter table
      Para adicionar
      alter table pessoas
       add colunm profissao varchar(10);

#  Removendo a coluna
    alter table pessoas
    drop colunm profissao

#  Alterando a posição da coluna
    alter table pessoas
    add colunm profissao varchar(10) after nome;

#  Alter table pessoas(comando para adicionar a coluna na primeira posição)
    add colunm codigo int fist

#  Modificando coluna
    alter table pessoas
    modify column  varchar(20) not null *default ''*;

#  Alterando o nome da coluna(sempre passas as contranges)
    alter table pessoas
    change column profissao prof varchar(20);

#  alterando o nome da tabelas
    alter table pessoas
    rename to gafanhotos

#  Criando uma nova tabelas
    create table if not exists cursos( //criar se não existir
        nome varchar(30) not null unique, //unique não deixa ele inserir dados repetidos
        descricao text,
        carga int unsignd, // para não negativos  
        totaulas int,
        ano year default '2018'
      )default charset = utf8;

#  Adicionando a PRIMARY
    alter table cursos
    add primary key (idcurso);

#  Apagando tabela
    drop table cursos

#  criando e adicioanndo uma colina id primeira posição e auto auto_incremente primery key

    alter table cursos drop column idCursos;
    alter table cursos add idCursos INT NOT NULL PRIMARY KEY auto_increment first;  


#  manipulando linha/tuplas
    Linha -> tuplas/registros
    Colunas -> campos/atributos

#  Modificando linhas incorretas
      update cursos set nome = 'HTML5' where id curso = '1'

      update cursos set nome = 'HTML5', ano = '2015' where id curso = '1'// utiliza a "," para alterar mais de uma paramentro

      update cursos set nome = 'HTML5' where id curso = '1' limit 1;

#  apagando linhas
      delete from cursos where "idCursos" = 8;

#  apagando todas as linhas
      truncate table cursos;


#  Gerenciando cópias de segurança MySql


#  Select 01
      seleciona tudo do tabelas
        select * from cursos;

      ordenando os dados da tabela
        order by nome_da_coluna;
      Ordenar por descrecente
        order by nome_da_coluna desc;
      Ordenar por crescente
        orde by nome_da_coluna asc;

#  select de algumas Colunas
        select nome, carga e ano from cursos;

#  select de linhas e colunas
        select * from cursos where = 'a linha que vc quer pesquisar'

        select nome, carga cursos where = 'a linha que vc quer'

#  select entre algum paramentro
        select nome, ano, from cursos where  nao between 2014 and 2016.

#  select de valores especifico
        select nome, descricao, ano from cursos where ano in(2014,2016,2017) order by;

#    select com operados logicos
        select * fom cursos where carga > 35 and totaulas < 30;


#       Boas praticas

            de TUNING, em minhas pesquisas
           relacionei algumas práticas que podem ser aplicadas na construção de consultas SQL:
            Evitar select *
            Cuidado com o as subqueries
            Troque o in pelo exists
            Evite o distinct consome muita memória
            Prefira o > ao invés do <>
            Use o between ao invés do >= ou <=
            Não faça cálculos matemáticos em colunas utilizadas no where
            Like é melhor que sbstr
            Prefira where com campos chaves"﻿

#   select com like
      select * from cursos where nome like 'p%';

#  select distinct
      select distinct nacionalidade from gafanhotos order by;

#  Agregação
        count// conta a quantidade de resultados
          select count(paramentro) from cursos;

        Max // o maximo do paramentro passado
          select max(paramentos) from tabela;

        SUM
          select sum(totaulas) from cursos where ano = '2016'

        AVG
          Select AVG(totaulas) from cursos where ano = '2016'


#  Agrupar
        Select carga from cursos
        group by carga;

        com Agregação
          select carga, count(nome) from
          group by totaulas
          order by totaulas;

        Com where
          select carga, count(nome) from
          group by totaulas
          order by totaulas;

          select carga, totaulas from cursos  where totaulas = 30
          group by carga;

        agrupando e agregando
          select carga, count(nome) from cursos
          group by carga
          having count(nome) > 3
          group by count(param) desc;

          select ano, count(*) from cursos
            where totaulas > 30
            group by ano
            having ano > 2013
            group by count(*) desc;

          select avg(carga) from cursos;

          select carga, count(cont) from cursos
            where ano > 2015
            group by carga
            having carga > (select avg(carga) from cursos);



#   Relacionando tabelas
      Modelo relacional
        Dados que se relacionam entre si

#   Chave Estrangeira
      São chaves primarias que são utilizadas em outras chaves de outra tabela. *chave primaria de alguem*

      Depende da classifica


#   Chaves estrangeiras e join
      ACID
        Atomicidade - Ou vai todos ou nao vai - caso o banco tenha que executar algum procedimento e não consiga realizar todo ele volta ao inicial.

        Consistência - Um banco ques estava conssitente deve estar ok no final da transação.

        Isolamento - quando tem duas trasações uma executa como se fosse isolado

# Adicionando a Chave estrangeira
      alter table gafanhotos add column cursopreferido int;

      Alter table gafanhotos
      add foreign key(cursopreferido)
      references cursos(idcurso)

# realacionando a chave estrangeira com a tabelas
      update gafanhotos set cursopreferido = '6' where id = '1';

#   utilidade da chave estrangeira
      select nome, cursopreferido from gafanhotos;
      select nome, ano from cursos;
      selct nome, cursopreferido from gafanhotos join curso
      on cursos.idcursos = gafanhotos.cursopreferido;

      select nome, cursopreferido from gafanhotos;
      select nome, ano from cursos;
      selct nome, cursopreferido from gafanhotos inner join curso
      on cursos.idcursos = gafanhotos.cursopreferido;

# Usando apelido
      select g.nome, c.nome, c.ano
      from gafanhotos as g join cursos as c on c.idcurso = g.cursopreferido
      order by g.nome;

# outer join
      select g.nome, c.cursopreferido from gafanhotos;
      select nome, ano from cursos;
      selct nome, cursopreferido from gafanhotos as ginner left(right) outer join curso as c
      on c.idcursos = g.cursopreferido;

# outro join
      select g.id, g.nome, c.nome, c.ano, c.descricao
      from gafanhotos as g left join cursos as c on c.idcurso = g.cursopreferido
      order by c.nome;
